mosquitto:
  pkg.installed:
    - pkgs:
      - mosquitto
      - mosquitto-clients
      - python-mosquitto
