java:
  file.managed:
    - name: /opt/jdk8.tar.gz
    - source: salt://jdk8.tar.gz
  cmd.run:
    - names:
      - tar xvf /opt/jdk8.tar.gz -C /opt/
      - update-alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_33/bin/javac 1
      - update-alternatives --install /usr/bin/java java /opt/jdk1.8.0_33/bin/java 1
      #- update-alternatives --config javac
      #- update-alternatives --config java
    - require:
      - file: java
