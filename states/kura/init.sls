kura:
  cmd.run:
    - name: dpkg -i /tmp/kura.deb
    - require:
      - file: /tmp/kura.deb
      - pkg: packages

/tmp/kura.deb:
 file.managed:
   - source: salt://kura1.2.0.deb

packages:
  pkg.installed:
    - pkgs:
      -  hostapd
      -  bind9
      -  isc-dhcp-server
      -  iw
      -  monit
      -  unzip
      -  dos2unix
      -  telnet
