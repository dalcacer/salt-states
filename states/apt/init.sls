/etc/apt/sources.list:
  file.managed:
    - source: salt://sources.template
    - template: jinja
    - defaults:
        url: {{ pillar['apt'] }}

/opt/mosquitto.key:
  file.managed:
    - source: salt://mosquitto.key

/opt/saltstack.key:
  file.managed:
    - source: salt://saltstack.key

apt:
  cmd.run:
    - names:
      - apt-key add /opt/mosquitto.key
      - apt-key add /opt/saltstack.key
      - rm /etc/apt/sources.list.d/*
    - require:
      - file: /etc/apt/sources.list
      - file: /opt/mosquitto.key
      - file: /opt/saltstack.key
